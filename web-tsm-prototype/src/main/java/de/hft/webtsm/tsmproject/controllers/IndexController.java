package de.hft.webtsm.tsmproject.controllers;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Rio
 */
@Named
@SessionScoped
public class IndexController extends BaseController implements Serializable {

    private String selectedTemplate = "/controls/controlDashboard.xhtml";

    public String getSelectedTemplate() {
        return selectedTemplate;
    }

    public void setSelectedTemplate(String selectedTemplate) {
        this.selectedTemplate = selectedTemplate;
    }

    public String manageUsers() {
        return "manageUsers";
    }

    public void navigateToControlViewUser() {
        selectedTemplate = "/controls/user/controlViewUsers.xhtml";
    }

    public void navigateToControlRegisterUser() {
        selectedTemplate = "/controls/user/controlRegisterUser.xhtml";
    }

    public void navigateToControlDeleteUser() {
        selectedTemplate = "/controls/user/controlDeleteUser.xhtml";
    }

    public void navigateToControlDashboard() {
        selectedTemplate = "/controls/controlDashboard.xhtml";
    }
}
