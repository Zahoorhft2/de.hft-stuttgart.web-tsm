/*
 * The MIT License
 *
 * Copyright 2014 (see authors.txt).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.hft.webtsm.tsmproject.controllers;

import de.hft.webtsm.tsmproject.entities.User;
import de.hft.webtsm.tsmproject.services.AuthentificationService;
import de.hft.webtsm.tsmproject.services.UserService;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Florian Bader
 */
@Named
@SessionScoped
public class LoginController extends BaseController implements Serializable {
    private @Inject UserService userService;
    private @Inject AuthentificationService authentificationService;
    
    private String userName;    
    private String userPassword;

    /**
     * Get the value of userPassword
     *
     * @return the value of userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * Set the value of userPassword
     *
     * @param userPassword new value of userPassword
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * Get the value of userName
     *
     * @return the value of userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Set the value of userName
     *
     * @param userName new value of userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 
     * @return 
     */
    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        
        User user = userService.getUserByName(userName);
        if (user == null) {            
            context.addMessage(null, new FacesMessage("User was not found or password is wrong."));
            return "";
        }
        
        Boolean isEqual = authentificationService.isEqual(
                userPassword, 
                user.getEncryptedPassword(), 
                user.getPasswordSalt());
        
        if (!isEqual) {            
            context.addMessage(null, new FacesMessage("User was not found or password is wrong."));
            return "";
        } else {
            return "";
        }
    }
}
