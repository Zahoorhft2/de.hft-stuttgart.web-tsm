/* 
 * The MIT License
 *
 * Copyright 2014 (see authors.txt).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.hft.webtsm.tsmproject.controllers;

import de.hft.webtsm.tsmproject.entities.User;
import de.hft.webtsm.tsmproject.services.AuthentificationService;
import de.hft.webtsm.tsmproject.services.UserService;
import de.hft.webtsm.tsmproject.utils.ValidationManager;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Florian Bader
 */
@Named
@SessionScoped
public class UserController extends BaseController implements Serializable {    
    private @Inject UserService userService;
    private @Inject AuthentificationService authentificationService;
    
    private User currentUser;    
    private List<User> currentUsers;
    private String password;

    /**
     *
     */
    @PostConstruct
    public void init() {
        LOG.info("UserController initialized.");
        currentUser = new User();
        currentUsers = userService.findWithNamedQuery(User.ALL);        
    }
    
    /**
     *
     * @return
     */
    public List<User> getCurrentUsers(){
        return this.currentUsers;
    }
    
    /**
     *
     * @return
     */
    public User getCurrentUser() {
        return currentUser;
    }

    /**
     *
     * @param user
     */
    public void setCurrentUser(User user) {
        this.currentUser = user;
    }
    
    /**
     *
     * @param user
     */
    public void deleteUser(User user) {
        //User foundUser = (User) userService.find(user);
        userService.delete(user);
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String tmpPassword) {
        this.password = tmpPassword;
    }
    
    /**
     *
     * @return
     */
    public String saveUser(){        
        FacesContext context = FacesContext.getCurrentInstance();
        
        byte[] passwordSalt = authentificationService.getRandomSalt();
        currentUser.setPasswordSalt(passwordSalt);
        
        byte[] encryptedPassword = authentificationService.getEncryptedPassword(password, passwordSalt);
        currentUser.setEncryptedPassword(encryptedPassword);
    	        
    	Boolean isValid = ValidationManager.isValid(currentUser);
        if (!isValid) {
            context.addMessage(null, new FacesMessage("Bla", "Bla Yolo") );
            return "";
        }       
        
        context.addMessage(null, new FacesMessage("Successful",  "You saved a new TSM USER =)") );
        User newUser = (User) userService.save(currentUser);
        if (newUser == null) {
            return "";
        }
        
        this.currentUsers.add(newUser);
        currentUser = new User();
        
        return "";
    }
}
