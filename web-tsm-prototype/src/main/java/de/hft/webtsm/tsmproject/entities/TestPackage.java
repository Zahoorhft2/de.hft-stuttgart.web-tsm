/*
 * The MIT License
 *
 * Copyright 2014 (see authors.txt).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.hft.webtsm.tsmproject.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Florian Bader
 */
@Entity(name = "test_packages")
@NamedQueries({
    @NamedQuery(name = TestPackage.ALL, query = "SELECT tp FROM test_packages tp"),
    @NamedQuery(name = TestPackage.ID, query = "SELECT tp FROM test_packages tp WHERE tp.id = :id"),
    @NamedQuery(name = TestPackage.NAME, query = "SELECT tp FROM test_packages tp WHERE tp.name = :name"),       
})
public class TestPackage extends BaseEntity implements Serializable {

    /**
     *
     */
    public final static String ALL = "TestPackage.findAll";

    /**
     *
     */
    public final static String ID = "TestPackage.findById";

    /**
     *
     */
    public final static String NAME = "TestPackage.findByName";
    
    @Column(length = 64, nullable = false)
    private String name;
        
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_project_id", nullable = false)
    private TestProject testProject;

    /**
     * Get the value of testProject
     *
     * @return the value of testProject
     */
    public TestProject getTestProject() {
        return testProject;
    }

    /**
     * Set the value of testProject
     *
     * @param testProject new value of testProject
     */
    public void setTestProject(TestProject testProject) {
        this.testProject = testProject;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }
}
