/*
 * The MIT License
 *
 * Copyright 2014 (see authors.txt).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package de.hft.webtsm.tsmproject.entities;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author Florian Bader
 */
@Entity(name = "test_projects")
@NamedQueries({
    @NamedQuery(name = TestProject.ALL, query = "SELECT tp FROM test_projects tp"),
    @NamedQuery(name = TestProject.ID, query = "SELECT tp FROM test_projects tp WHERE tp.id = :id"),
    @NamedQuery(name = TestProject.NAME, query = "SELECT tp FROM test_projects tp WHERE tp.name = :name"),       
})
public class TestProject extends BaseEntity implements Serializable {

    /**
     *
     */
    public final static String ALL = "TestProject.findAll";

    /**
     *
     */
    public final static String ID = "TestProject.findById";

    /**
     *
     */
    public final static String NAME = "TestProject.findByName";
    
    @Column(length = 64, nullable = false)    
    private String name;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "testProject")
    private Set<TestPackage> testPackages;
    
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "test_project_users", 
        joinColumns = { @JoinColumn(name = "user_id", nullable = false, updatable = false) }, 
        inverseJoinColumns = { @JoinColumn(name = "test_project_id", nullable = false, updatable = false) })
    private Set<User> projectUsers;

    /**
     * Get the value of projectUsers
     *
     * @return the value of projectUsers
     */
    public Set<User> getProjectUsers() {
        return projectUsers;
    }

    /**
     * Set the value of projectUsers
     *
     * @param projectUsers new value of projectUsers
     */
    public void setProjectUsers(Set<User> projectUsers) {
        this.projectUsers = projectUsers;
    }
    
    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the value of testPackages
     *
     * @return the value of testPackages
     */
    public Set<TestPackage> getTestPackages() {
        return testPackages;
    }

    /**
     * Set the value of testPackages
     *
     * @param testPackages new value of testPackages
     */
    public void setTestPackages(Set<TestPackage> testPackages) {
        this.testPackages = testPackages;
    }
}
