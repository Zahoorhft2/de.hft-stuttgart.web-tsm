/* 
 * The MIT License
 *
 * Copyright 2014 (see authors.txt).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.hft.webtsm.tsmproject.entities;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Jan
 * @author Florian Bader
 */
@Entity(name = "users")
@NamedQueries({
    @NamedQuery(name = User.ALL, query = "SELECT u FROM users u"),
    @NamedQuery(name = User.ID, query = "SELECT u FROM users u WHERE u.id = :id"),
    @NamedQuery(name = User.MAIL, query = "SELECT u FROM users u WHERE u.email = :email"),
    @NamedQuery(name = User.NAME, query = "SELECT u FROM users u WHERE u.name = :name"),
       
})
public class User extends BaseEntity implements Serializable {

    /**
     *
     */
    public final static String ALL = "User.findAll";

    /**
     *
     */
    public final static String ID = "User.findById";

    /**
     *
     */
    public final static String MAIL = "User.findByMail";

    /**
     *
     */
    public final static String NAME = "User.findByName";
       
    @NotNull
    @Column(length = 64, nullable = false)
    private String name;
    
    @NotNull
    @Email
    @Column(length = 255, nullable = false)
    private String email;
    
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "test_project_users", 
        joinColumns = { @JoinColumn(name = "test_project_id", nullable = false, updatable = false) }, 
        inverseJoinColumns = { @JoinColumn(name = "user_id", nullable = false, updatable = false) })
    private Set<TestProject> testProjects;
    
    @NotNull
    @Column(name = "encrypted_password", nullable = false)
    private byte[] encryptedPassword;
    
    @NotNull
    @Column(name = "password_salt", nullable = false)
    private byte[] passwordSalt;

    /**
     * Get the value of passwordSalt
     *
     * @return the value of passwordSalt
     */
    public byte[] getPasswordSalt() {
        return passwordSalt;
    }

    /**
     * Set the value of passwordSalt
     *
     * @param passwordSalt new value of passwordSalt
     */
    public void setPasswordSalt(byte[] passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

    /**
     * Get the value of encryptedPassword
     *
     * @return the value of encryptedPassword
     */
    public byte[] getEncryptedPassword() {
        return encryptedPassword;
    }

    /**
     * Set the value of encryptedPassword
     *
     * @param encryptedPassword new value of encryptedPassword
     */
    public void setEncryptedPassword(byte[] encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
    
    /**
     * Get the value of testProjects
     *
     * @return the value of testProjects
     */
    public Set<TestProject> getTestProjects() {
        return testProjects;
    }

    /**
     * Set the value of testProjects
     *
     * @param testProjects new value of testProjects
     */
    public void setTestProjects(Set<TestProject> testProjects) {
        this.testProjects = testProjects;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Get the value of email
     *
     * @return the value of email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the value of email
     *
     * @param email new value of email
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
