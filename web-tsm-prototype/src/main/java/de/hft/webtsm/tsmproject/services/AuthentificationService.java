/*
 * The MIT License
 *
 * Copyright 2014 (see authors.txt).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.hft.webtsm.tsmproject.services;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.ejb.Stateless;

/**
 * Encrypts passwords with the PBKDF2 encryption algorithm and also creates
 * random salts.
 *
 * @author Florian Bader
 */
@Stateless
public class AuthentificationService {
    private static final Logger LOG = Logger.getLogger(AuthentificationService.class.getName());
    private static final String ENCRYPTION_ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final int ENCRYPTION_ITERATIONS = 10000;
    private static final int DERIVED_KEY_LENGTH = 160;
    private static final int SALT_LENGTH = 8;
    
    /**
     *
     * @param password
     * @param encryptedPassword
     * @param salt
     * @return
     */
    public Boolean isEqual(String password, byte[] encryptedPassword, byte[] salt) {
        byte[] encryptedAttemptedPassword = getEncryptedPassword(password, salt);
        return Arrays.equals(encryptedPassword, encryptedAttemptedPassword);
    }
        
    /**
     *
     * @param password
     * @param salt
     * @return
     */
    public byte[] getEncryptedPassword(String password, byte[] salt) {
        try {
            KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt,
                    ENCRYPTION_ITERATIONS, DERIVED_KEY_LENGTH);
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(ENCRYPTION_ALGORITHM);

            return secretKeyFactory.generateSecret(keySpec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     *
     * @return
     */
    public byte[] getRandomSalt() {
        try {
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            
            byte[] salt = new byte[SALT_LENGTH];
            random.nextBytes(salt);
            
            return salt;
        } catch (NoSuchAlgorithmException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
