/* 
 * The MIT License
 *
 * Copyright 2014 (see authors.txt).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
import de.hft.webtsm.tsmproject.entities.User;
import de.hft.webtsm.tsmproject.utils.ValidationManager;
import java.security.SecureRandom;
import java.util.Random;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Jan
 */
public class ValidationTests {

    /**
     *
     */
    public ValidationTests() {
    }

    /**
     *
     */
    @Test
    public void userValidationTest() {
        User user1 = new User();
        
        Random random = new SecureRandom();
        byte[] randomBytes = new byte[16];
        random.nextBytes(randomBytes);
        
        
        user1.setName("Jamnes Bond");
        user1.setEmail("JB@MI6.uk");
        user1.setEncryptedPassword(randomBytes);
        user1.setPasswordSalt(randomBytes);
        assertTrue(ValidationManager.isValid(user1));
        assertTrue(ValidationManager.returnAmountOfInvalidFields(user1).isEmpty());

        user1.setName(null);
        user1.setEmail("JB@MI6.uk");
        user1.setEncryptedPassword(randomBytes);
        user1.setPasswordSalt(randomBytes);
        assertTrue(!ValidationManager.isValid(user1));
        assertTrue(ValidationManager.returnAmountOfInvalidFields(user1).size() == 1);
        assertTrue(ValidationManager.returnAmountOfInvalidFields(user1).contains("name"));

        user1.setName("Jamnes Bond");
        user1.setEmail(null);
        user1.setEncryptedPassword(randomBytes);
        user1.setPasswordSalt(randomBytes);
        assertTrue(!ValidationManager.isValid(user1));
        assertTrue(ValidationManager.returnAmountOfInvalidFields(user1).size() == 1);
        assertTrue(ValidationManager.returnAmountOfInvalidFields(user1).contains("email"));

        user1.setName(null);
        user1.setEmail(null);
        user1.setEncryptedPassword(randomBytes);
        user1.setPasswordSalt(randomBytes);
        assertTrue(!ValidationManager.isValid(user1));
        assertTrue(ValidationManager.returnAmountOfInvalidFields(user1).size() == 2);
        assertTrue(ValidationManager.returnAmountOfInvalidFields(user1).contains("name")
                && ValidationManager.returnAmountOfInvalidFields(user1).contains("email"));
        
        user1.setName("James Bond");
        user1.setEmail("JB@MI6.uk");
        user1.setEncryptedPassword(null);
        user1.setPasswordSalt(null);
        assertTrue(!ValidationManager.isValid(user1));
        assertTrue(ValidationManager.returnAmountOfInvalidFields(user1).size() == 2);
        assertTrue(ValidationManager.returnAmountOfInvalidFields(user1).contains("Password")
                && ValidationManager.returnAmountOfInvalidFields(user1).contains("Salt"));
        
        
    }
}
